import { NgModule } from '@angular/core';
import { HomeComponent } from '../../components/home/home.component';
import { SearchComponent } from '../../components/home/search/search.component';
import { SharedModule } from '../shared/shared-module';
import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  declarations: [HomeComponent, SearchComponent],
  imports: [SharedModule, HomeRoutingModule],
})
export class HomeModule {}
