import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from '../../components/home/details/details.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserModalComponent } from '../../components/user-modal/user-modal.component';
import { ConfirmationModalComponent } from '../../components/shared/confirmation-modal/confirmation-modal.component';
import { ErrorComponent } from '../../components/error/error.component';

@NgModule({
  declarations: [
    DetailsComponent,
    UserModalComponent,
    ConfirmationModalComponent,
    ErrorComponent,
  ],
  imports: [
    CommonModule,
    AutocompleteLibModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
  ],
  exports: [
    CommonModule,
    AutocompleteLibModule,
    ReactiveFormsModule,
    DetailsComponent,
    NgbModule,
    HttpClientModule,
    UserModalComponent,
    ConfirmationModalComponent,
    ErrorComponent,
  ],
  providers: [NgbActiveModal],
})
export class SharedModule {}
