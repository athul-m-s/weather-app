import { NgModule } from '@angular/core';
import { FavoriteComponent } from '../../components/favorite/favorite.component';
import { SharedModule } from '../shared/shared-module';
import { FavoritesRoutingModule } from './favorites-routing.module';

@NgModule({
  declarations: [FavoriteComponent],
  imports: [SharedModule, FavoritesRoutingModule],
})
export class FavoritesModule {}
