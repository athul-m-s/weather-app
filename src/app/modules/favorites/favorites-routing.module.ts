import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoriteComponent } from '../../components/favorite/favorite.component';
import { AuthGuard } from '../../services/auth/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: FavoriteComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoritesRoutingModule {}
