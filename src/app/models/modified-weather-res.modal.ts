export interface ModifiedResponse {
  city: string;
  weatherDescription: string;
  temp: string;
  feels_like: string;
  temp_min: string;
  temp_max: string;
  pressure: string;
  humidity: string;
  windSpeed: string;
  rain: boolean;
  icon: string;
  time: string;
}
