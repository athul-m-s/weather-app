export interface cities {
  id: number;
  name: string;
  state: string;
  country: string;
  coord: coord;
}

export interface coord {
  lon: number;
  lat: number;
}
