import { TestBed } from '@angular/core/testing';

import { AuthGuard } from './auth-guard.service';
import { SharedModule } from '../../modules/shared/shared-module';
import { RouterTestingModule } from '@angular/router/testing';
import { WeatherService } from '../weather.service';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AuthGuardService', () => {
  let guard: AuthGuard;

  let service: WeatherService;
  //mock data start
  let routeMock: any = { snapshot: {} };
  let routeStateMock: any = { snapshot: {}, url: '/favorites' };
  let routerMock = { navigate: jasmine.createSpy('navigate') };
  //mock data end

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes([]),
        HttpClientTestingModule,
      ],
      providers: [AuthGuard, { provide: Router, useValue: routerMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthGuard);
    service = TestBed.inject(WeatherService);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should redirect an non-user to the home route', () => {
    expect(guard.canActivate(routeMock, routeStateMock)).toEqual(false);
    expect(routerMock.navigate).toHaveBeenCalledWith(['/home']);
  });

  it('should allow the authenticated user to access app', () => {
    spyOn(service, 'getUserName').and.returnValue('xyz');
    expect(guard.canActivate(routeMock, routeStateMock)).toEqual(true);
  });
});
