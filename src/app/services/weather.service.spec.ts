import { TestBed } from '@angular/core/testing';

import { WeatherService } from './weather.service';
import { SharedModule } from '../modules/shared/shared-module';
import { WeatherResponse } from '../models/weather-response.modal';
import { cities, coord } from '../models/cities.modal';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('WeatherService', () => {
  let httpMock: HttpTestingController;
  let service: WeatherService;

  let response: WeatherResponse[] = [
    {
      coord: {
        lon: 76.65,
        lat: 10.7833,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      base: 'stations',
      main: {
        temp: 26.32,
        feels_like: 26.32,
        temp_min: 26.32,
        temp_max: 31.03,
        pressure: 1008,
        humidity: 93,
      },
      visibility: 10000,
      wind: {
        speed: 5.66,
        deg: 255,
      },
      rain: {
        '1h': 2.05,
      },
      clouds: {
        all: 100,
      },
      dt: 1630915343,
      sys: {
        type: 2,
        id: 2040581,
        country: 'IN',
        sunrise: 1630889021,
        sunset: 1630933221,
        message: 123,
      },
      timezone: 19800,
      id: 1260728,
      name: 'Palakkad',
      cod: 200,
      time: '2021-09-06T08:02:24.112Z',
    },
    {
      coord: {
        lon: 76.2144,
        lat: 10.5276,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      base: 'stations',
      main: {
        temp: 27.94,
        feels_like: 32.73,
        temp_min: 26.77,
        temp_max: 27.94,
        pressure: 1009,
        humidity: 85,
      },
      visibility: 10000,
      wind: {
        speed: 1.78,
        deg: 283,
      },
      rain: {
        '1h': 3.16,
      },
      clouds: {
        all: 100,
      },
      dt: 1630915349,
      sys: {
        type: 1,
        id: 9211,
        country: 'IN',
        sunrise: 1630889133,
        sunset: 1630933318,
        message: 123,
      },
      timezone: 19800,
      id: 1254187,
      name: 'Thrissur',
      cod: 200,
      time: '2021-09-06T08:02:29.490Z',
    },
  ];

  let responseCity: cities[] = [
    {
      id: 7289568,
      name: 'Palakkad district',
      state: '',
      country: 'IN',
      coord: {
        lon: 76.651001,
        lat: 10.775,
      },
    },
    {
      id: 1277333,
      name: 'Bengaluru',
      state: '',
      country: 'IN',
      coord: {
        lon: 77.603287,
        lat: 12.97623,
      },
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule, HttpClientTestingModule],
      providers: [WeatherService],
    }).compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeatherService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Should remove favorites city details when removeAllFavorites calls', () => {
    service.favorites = response;
    service.removeAllFavorites();
    expect(service.favorites.length).toBe(0);
  });

  it('Should add new city info to favorite when setFavorites called', () => {
    spyOn(service, 'isAlreadyExists').and.returnValue(-1);
    service.favorites.push(response[0]);
    service.setFavorites(response[1]);
    expect(service.favorites).toContain(response[1]);
  });

  it('Should not add same city details multiple times into favoriteCities', () => {
    service.favorites = response;
    let result = service.isAlreadyExists(response[0]);
    expect(result).toEqual(0);
  });

  it('Should delete city details with specified index from favoriteCities ', () => {
    service.favorites = response;
    service.deleteFavorites(1);
    expect(service.favorites.length).toEqual(1);
  });

  it('Should return cities list when getCities() executed', () => {
    let data = responseCity;
    service.getCities().subscribe((res) => {
      expect(res).toEqual(data);
    });

    const req = httpMock.expectOne('../../assets/city.list.json');
    expect(req.request.method).toEqual('GET');
    req.flush(data);

    httpMock.verify();
  });
});
