import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { cities } from '../models/cities.modal';
import { WeatherResponse } from '../models/weather-response.modal';
import { Coordinates } from '../models/coordinates.modal';

@Injectable({
  providedIn: 'root',
})
export class WeatherService {
  favorites: WeatherResponse[] = [];
  userName: string = '';
  weatherUpdateSub = new Subject<WeatherResponse>();
  errorUpdateSub = new Subject<string>();
  constructor(private http: HttpClient) {}

  //service calls

  getCities() {
    return this.http.get<cities[]>('../../assets/city.list.json').pipe(
      map((cities) => {
        return cities;
      }),
      catchError((error) => {
        return throwError(error);
      })
    );
  }

  getWeatherInfo(city: string) {
    return this.http
      .get<WeatherResponse>(
        `${environment.api}q=${city}&units=metric&appid=${environment.appKey}`
      )
      .pipe(
        catchError((err) => {
          return throwError(err);
        }),
        map((res) => {
          res.time = new Date().toString();
          return res;
        })
      );
  }

  getWeatherInfoByCoordinates(coordinates: Coordinates) {
    return this.http
      .get<any>(
        `${environment.api}lat=${coordinates.lat}&lon=${coordinates.lon}&units=metric&appid=${environment.appKey}`
      )
      .pipe(
        catchError((err) => {
          return throwError(err);
        }),
        map((res) => {
          res.time = new Date().toString();
          return res;
        })
      );
  }

  //favorites

  removeAllFavorites() {
    this.favorites = [];
  }

  getFavorites(): WeatherResponse[] {
    return this.favorites;
  }

  setFavorites(item: WeatherResponse) {
    if (this.isAlreadyExists(item) == -1) {
      this.favorites.push(item);
    }
  }

  isAlreadyExists(item: WeatherResponse) {
    return this.favorites.findIndex((city) => city.name == item.name);
  }

  deleteFavorites(indx: number) {
    this.favorites.splice(indx, 1);
  }

  updateFavorites(favoriteCityRes: WeatherResponse, index: number) {
    this.favorites[index] = favoriteCityRes;
  }

  //username

  getUserName() {
    return this.userName;
  }

  setUserName(name: string) {
    this.userName = name;
  }

  triggerWeatherInfo(data: WeatherResponse) {
    this.weatherUpdateSub.next(data);
  }

  triggerErrorInfo(error: string) {
    this.errorUpdateSub.next(error);
  }
}
