import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css'],
})
export class UserModalComponent implements OnInit, OnDestroy {
  registerFormGroup!: FormGroup;
  username!: string;
  city!: string;

  private subscription!: Subscription;

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private weatherService: WeatherService,
    private modalService: NgbModal,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.createIntroFormGroup();
  }

  createIntroFormGroup() {
    this.registerFormGroup = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
      city: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
    });
  }

  onSubmit() {
    this.updateName();
    this.updateWeatherInfo();
  }

  updateName() {
    this.username = this.registerFormGroup.get('name')?.value;
    this.weatherService.setUserName(this.username.trim());
    this.updateSession();
  }

  updateWeatherInfo() {
    this.city = this.registerFormGroup.get('city')?.value;
    this.subscription = this.weatherService
      .getWeatherInfo(this.city.trim())
      .subscribe(
        (data) => {
          if (data) {
            this.registerFormGroup.reset();
            this.weatherService.triggerWeatherInfo(data);
            this.router.navigate(['/home']);
            this.activeModal.close();
          }
        },
        (err: HttpErrorResponse) => {
          this.updateName();
          let error = err?.error.message;
          this.weatherService.triggerErrorInfo(error ? error : 'unknown error');
          this.router.navigate(['/home']);
          this.activeModal.close();
        }
      );
  }

  updateSession() {
    let obj = this.createUserObject();
    sessionStorage.setItem('user', JSON.stringify(obj));
  }

  createUserObject() {
    return {
      name: this.username,
    };
  }

  ngOnDestroy() {
    this.subscription ? this.subscription.unsubscribe() : null;
  }
}
