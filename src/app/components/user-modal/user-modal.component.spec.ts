import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserModalComponent } from './user-modal.component';
import { SharedModule } from '../../modules/shared/shared-module';
import { RouterTestingModule } from '@angular/router/testing';
import { MockLocationStrategy } from '@angular/common/testing';
import { By } from '@angular/platform-browser';
import { WeatherService } from '../../services/weather.service';
import { of, throwError } from 'rxjs';
import { WeatherResponse } from '../../models/weather-response.modal';
import { Router } from '@angular/router';
import { LocationStrategy } from '@angular/common';

describe('UserModalComponent', () => {
  let component: UserModalComponent;
  let fixture: ComponentFixture<UserModalComponent>;

  let response: WeatherResponse[] = [
    {
      coord: {
        lon: 76.65,
        lat: 10.7833,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      base: 'stations',
      main: {
        temp: 26.32,
        feels_like: 26.32,
        temp_min: 26.32,
        temp_max: 31.03,
        pressure: 1008,
        humidity: 93,
      },
      visibility: 10000,
      wind: {
        speed: 5.66,
        deg: 255,
      },
      rain: {
        '1h': 2.05,
      },
      clouds: {
        all: 100,
      },
      dt: 1630915343,
      sys: {
        type: 2,
        id: 2040581,
        country: 'IN',
        sunrise: 1630889021,
        sunset: 1630933221,
        message: 123,
      },
      timezone: 19800,
      id: 1260728,
      name: 'Palakkad',
      cod: 200,
      time: '2021-09-06T08:02:24.112Z',
    },
  ];

  class RouterStub {
    navigate(params: any) {}
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserModalComponent],
      imports: [SharedModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provider: Router,
          useClass: RouterStub,
        },
        { provide: LocationStrategy, useClass: MockLocationStrategy },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a form with two control', () => {
    expect(component.registerFormGroup.contains('city')).toBeTruthy();
    expect(component.registerFormGroup.contains('name')).toBeTruthy();
  });

  it('should be a invalid form if invalid data entered', () => {
    component.registerFormGroup.get('city')?.setValue('');
    component.registerFormGroup.get('name')?.setValue('');
    expect(component.registerFormGroup.valid).toBeFalsy();
  });

  it('Should be valid if user entered proper values', () => {
    component.registerFormGroup.get('city')?.setValue('xyz');
    component.registerFormGroup.get('name')?.setValue('bca');
    expect(component.registerFormGroup.valid).toBeTruthy();
  });

  it('should disable submit button initially', () => {
    let btn = fixture.debugElement.query(By.css('#btn-save'));
    expect(btn.nativeElement.disabled).toBeTruthy();
  });

  it('should not call onSubmit method when proper values are not entered', () => {
    spyOn(component, 'onSubmit');
    let el = fixture.debugElement.query(By.css('#btn-save')).nativeElement;
    el.click();
    expect(component.onSubmit).toHaveBeenCalledTimes(0);
  });

  it('should call onSubmit method when proper values are entered', () => {
    spyOn(component, 'onSubmit');
    component.registerFormGroup.get('city')?.setValue('xyz');
    component.registerFormGroup.get('name')?.setValue('bca');
    fixture.detectChanges();

    let el = fixture.debugElement.query(By.css('#btn-save')).nativeElement;
    el.click();
    expect(component.onSubmit).toHaveBeenCalledTimes(1);
  });

  it('Should update username when user clicks save', () => {
    component.registerFormGroup.get('name')?.setValue('bca');
    const service = TestBed.inject(WeatherService);
    spyOn(service, 'setUserName');
    component.updateName();
    expect(service.setUserName).toHaveBeenCalled();
  });

  it('should call updateWeatherInfo when user clicks save', () => {
    let router = TestBed.inject(Router);
    const service = TestBed.inject(WeatherService);
    let spy = spyOn(router, 'navigate');
    spyOn(service, 'getWeatherInfo').and.returnValue(of(response[0]));
    component.onSubmit();
    expect(service.getWeatherInfo).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(['/home']);
  });

  it('Should call redirect to home when getWeatherInfo return error', () => {
    let router = TestBed.inject(Router);
    const service = TestBed.inject(WeatherService);
    component.registerFormGroup.get('city')?.setValue('palakkad');
    const testError = {
      status: 404,
      error: {
        message: 'city not found',
      },
    };
    spyOn(service, 'getWeatherInfo').and.returnValue(throwError(testError));
    spyOn(service, 'triggerErrorInfo');
    spyOn(component, 'updateName');
    let spy = spyOn(router, 'navigate');

    component.updateWeatherInfo();

    expect(component.updateName).toHaveBeenCalled();
    expect(service.triggerErrorInfo).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(['/home']);
  });
});
