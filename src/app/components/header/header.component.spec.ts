import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { SharedModule } from '../../modules/shared/shared-module';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, RouterLinkWithHref } from '@angular/router';
import { By } from '@angular/platform-browser';
import { WeatherService } from '../../services/weather.service';
import { MockLocationStrategy } from '@angular/common/testing';
import { LocationStrategy } from '@angular/common';

class RouterStub {
  navigate(params: any) {}
}

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let service: WeatherService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [SharedModule, RouterTestingModule.withRoutes([])],
      providers: [
        WeatherService,
        { provide: LocationStrategy, useClass: MockLocationStrategy },
        {
          provider: Router,
          useClass: RouterStub,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(WeatherService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a link to home page ', () => {
    let de = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));

    let index = de.findIndex((de) => de.properties['pathname'] === '/home');
    expect(index).toBeGreaterThan(-1);
  });

  it('should have a link to favoritePage', () => {
    spyOn(component, 'getUserName').and.returnValue('xyz');
    fixture.detectChanges();
    let de = fixture.debugElement.queryAll(By.directive(RouterLinkWithHref));

    let index = de.findIndex(
      (de) => de.properties['pathname'] === '/favorites'
    );
    expect(index).toBeGreaterThan(-1);
  });

  it('should call logout method when user clicks logout icon', () => {
    spyOn(component, 'getUserName').and.returnValue('xyz');
    fixture.detectChanges();

    spyOn(component, 'logout');
    let el = fixture.debugElement.query(By.css('#btn-logout')).nativeElement;
    el.click();
    expect(component.logout).toHaveBeenCalled();
  });

  it('should call open method when user clicks Guest user', () => {
    spyOn(component, 'open');

    let el = fixture.debugElement.query(By.css('#btn-open')).nativeElement;
    el.click();
    expect(component.open).toHaveBeenCalled();
  });

  it('Should reset username and favorite cities when logout called', () => {
    let router = TestBed.inject(Router);
    spyOn(service, 'setUserName');
    spyOn(service, 'removeAllFavorites');
    let spy = spyOn(router, 'navigate');

    component.logout();

    expect(service.setUserName).toHaveBeenCalled();
    expect(service.removeAllFavorites).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith(['/home']);
  });
});
