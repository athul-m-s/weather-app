import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserModalComponent } from '../user-modal/user-modal.component';
import { WeatherService } from '../../services/weather.service';
import { WeatherResponse } from '../../models/weather-response.modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private modalService: NgbModal,
    private WeatherService: WeatherService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  open() {
    const modalRef = this.modalService.open(UserModalComponent, {
      windowClass: 'myCustomModalClass',
    });
  }

  getUserName(): string {
    return this.WeatherService.getUserName();
  }

  logout() {
    this.WeatherService.setUserName('');
    this.WeatherService.removeAllFavorites();
    this.WeatherService.weatherUpdateSub.next(undefined);
    sessionStorage.removeItem('user');
    this.router.navigate(['/home']);
  }
}
