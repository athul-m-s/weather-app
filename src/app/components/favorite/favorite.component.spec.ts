import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteComponent } from './favorite.component';
import { SharedModule } from '../../modules/shared/shared-module';
import { WeatherService } from '../../services/weather.service';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { WeatherResponse } from '../../models/weather-response.modal';
import { of } from 'rxjs';

describe('FavoriteComponent', () => {
  let component: FavoriteComponent;
  let fixture: ComponentFixture<FavoriteComponent>;
  let service: WeatherService;

  let response: WeatherResponse[] = [
    {
      coord: {
        lon: 76.65,
        lat: 10.7833,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      base: 'stations',
      main: {
        temp: 26.32,
        feels_like: 26.32,
        temp_min: 26.32,
        temp_max: 31.03,
        pressure: 1008,
        humidity: 93,
      },
      visibility: 10000,
      wind: {
        speed: 5.66,
        deg: 255,
      },
      rain: {
        '1h': 2.05,
      },
      clouds: {
        all: 100,
      },
      dt: 1630915343,
      sys: {
        type: 2,
        id: 2040581,
        country: 'IN',
        sunrise: 1630889021,
        sunset: 1630933221,
        message: 123,
      },
      timezone: 19800,
      id: 1260728,
      name: 'Palakkad',
      cod: 200,
      time: '2021-09-06T08:02:24.112Z',
    },
    {
      coord: {
        lon: 76.2144,
        lat: 10.5276,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      base: 'stations',
      main: {
        temp: 27.94,
        feels_like: 32.73,
        temp_min: 26.77,
        temp_max: 27.94,
        pressure: 1009,
        humidity: 85,
      },
      visibility: 10000,
      wind: {
        speed: 1.78,
        deg: 283,
      },
      rain: {
        '1h': 3.16,
      },
      clouds: {
        all: 100,
      },
      dt: 1630915349,
      sys: {
        type: 1,
        id: 9211,
        country: 'IN',
        sunrise: 1630889133,
        sunset: 1630933318,
        message: 123,
      },
      timezone: 19800,
      id: 1254187,
      name: 'Thrissur',
      cod: 200,
      time: '2021-09-06T08:02:29.490Z',
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FavoriteComponent],
      imports: [SharedModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(WeatherService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render No favorites when there is no favorite cities', () => {
    let h1 = fixture.nativeElement.querySelector('h3');
    expect(h1.textContent).toContain(component.message);
  });

  it('should render favorite city details in template', () => {
    spyOnProperty(component, 'favorites').and.returnValue(response);
    fixture.detectChanges();
    const el: DebugElement[] = fixture.debugElement.queryAll(
      By.css('.loopingArray')
    );

    expect(el.length).toEqual(response.length);
  });

  it('Should call getWeatherInfo when user clicks update button', () => {
    spyOn(service, 'getWeatherInfo').and.returnValue(of(response[0]));
    component.updateFavorites(response[0], 1);
    expect(service.getWeatherInfo).toHaveBeenCalled();
  });

  it('should call removeFavorites method when user clicks remove button', () => {
    spyOnProperty(component, 'favorites').and.returnValue(response);
    spyOn(component, 'removeFavorites');
    fixture.detectChanges();

    let el = fixture.debugElement.query(By.css('#btn-remove')).nativeElement;
    el.click();
    expect(component.removeFavorites).toHaveBeenCalled();
  });
});
