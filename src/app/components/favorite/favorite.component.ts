import { Component, OnDestroy, OnInit, Output } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { WeatherResponse } from '../../models/weather-response.modal';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from '../shared/confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
})
export class FavoriteComponent implements OnInit, OnDestroy {
  private subscription!: Subscription;

  message: string = 'No Favorites !!';

  constructor(
    private WeatherService: WeatherService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {}

  get favorites(): WeatherResponse[] {
    return this.WeatherService.getFavorites();
  }

  updateFavorites(favoriteCity: WeatherResponse, index: number) {
    this.subscription = this.WeatherService.getWeatherInfo(
      favoriteCity.name
    ).subscribe((data) => {
      this.WeatherService.updateFavorites(data, index);
    });
  }

  removeFavorites(index: number) {
    const modalRef = this.modalService.open(ConfirmationModalComponent, {
      windowClass: 'myCustomModalClass',
    });
    modalRef.componentInstance.outputData.subscribe((res: boolean) => {
      if (res) {
        this.WeatherService.deleteFavorites(index);
        modalRef.close();
      } else {
        modalRef.close();
      }
    });
  }

  ngOnDestroy() {
    this.subscription ? this.subscription.unsubscribe() : null;
  }
}
