import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationModalComponent } from './confirmation-modal.component';
import { SharedModule } from '../../../modules/shared/shared-module';

describe('ConfirmationModalComponent', () => {
  let component: ConfirmationModalComponent;
  let fixture: ComponentFixture<ConfirmationModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConfirmationModalComponent],
      imports: [SharedModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should emit true if user clicks OK button', () => {
    let flag = null;
    component.outputData.subscribe((x) => (flag = x));
    component.accept();

    expect(flag).toBeTruthy();
  });

  it('Should emit false if user clicks CANCEL button', () => {
    let flag = null;
    component.outputData.subscribe((x) => (flag = x));
    component.cancel();

    expect(flag).toBeFalsy();
  });
});
