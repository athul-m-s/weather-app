import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { SharedModule } from '../../modules/shared/shared-module';
import { SearchComponent } from './search/search.component';
import { DetailsComponent } from './details/details.component';
import { WeatherService } from '../../services/weather.service';
import { of } from 'rxjs';
import { cities } from '../../models/cities.modal';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let service: WeatherService;
  let response: cities[] = [
    {
      id: 7289568,
      name: 'Palakkad district',
      state: '',
      country: 'IN',
      coord: {
        lon: 76.651001,
        lat: 10.775,
      },
    },
    {
      id: 1277333,
      name: 'Bengaluru',
      state: '',
      country: 'IN',
      coord: {
        lon: 77.603287,
        lat: 12.97623,
      },
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent, SearchComponent, DetailsComponent],
      imports: [SharedModule],
    }).compileComponents();
  });

  beforeEach(() => {
    //mocked session start
    let store: any = {};
    const mockSessionStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      },
    };
    //mocked session end

    spyOn(sessionStorage, 'getItem').and.callFake(mockSessionStorage.getItem);
    spyOn(sessionStorage, 'setItem').and.callFake(mockSessionStorage.setItem);
    spyOn(sessionStorage, 'removeItem').and.callFake(
      mockSessionStorage.removeItem
    );
    spyOn(sessionStorage, 'clear').and.callFake(mockSessionStorage.clear);

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(WeatherService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should get cities details', () => {
    let service = TestBed.inject(WeatherService);
    spyOn(service, 'getCities').and.returnValue(of(response));

    component.ngOnInit();

    expect(component.cities.length).toBe(2);
  });

  it('should return stored user details from sessionStorage', () => {
    spyOn(service, 'setUserName');
    const user = { name: 'xyz' };
    sessionStorage.setItem('user', JSON.stringify(user));

    component.updateUserDetails();

    expect(service.setUserName).toHaveBeenCalled();
  });
});
