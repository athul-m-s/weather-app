import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { WeatherResponse } from '../../../models/weather-response.modal';
import { ModifiedResponse } from '../../../models/modified-weather-res.modal';
import { WeatherService } from '../../../services/weather.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() data!: WeatherResponse;
  searchResponse!: ModifiedResponse;
  currentTime!: string;
  homePage: boolean = false;
  favoritePage: boolean = false;
  showSuccessMessage: boolean = false;

  subscription!: Subscription;
  updateSubscription!: Subscription;

  constructor(private WeatherService: WeatherService) {}

  ngOnInit(): void {
    this.subscription = this.WeatherService.weatherUpdateSub.subscribe(
      (data) => {
        if (location.pathname == '/home') {
          this.data = data;
          this.processData();
        }
      },
      (err) => {}
    );
  }

  ngOnChanges() {
    this.setInitialValues();
    this.processData();
    this.resetFavoriteMessage();
  }

  processData() {
    this.searchResponse = {} as ModifiedResponse;
    this.searchResponse.city = this.data?.name;
    this.searchResponse.feels_like = this.data?.main.feels_like + '°C';
    this.searchResponse.humidity = this.data?.main.humidity + '%';
    this.searchResponse.pressure = this.data?.main.pressure + ' ' + 'hPa';
    this.searchResponse.temp = this.data?.main.temp + '°C';
    this.searchResponse.temp_max = this.data?.main.temp_max + '°C';
    this.searchResponse.temp_min = this.data?.main.temp_min + '°C';
    this.searchResponse.weatherDescription = this.data?.weather[0].description;
    this.searchResponse.windSpeed = this.data?.wind.speed + ' ' + 'm/s';
    this.searchResponse.rain = this.data?.rain ? true : false;
    this.searchResponse.icon = this.data?.weather[0].icon;
    this.searchResponse.time = this.data?.time.toString();
  }

  setInitialValues() {
    if (location.pathname == '/home') {
      this.homePage = true;
    } else {
      this.favoritePage = true;
    }
  }

  addToFavorites() {
    this.WeatherService.setFavorites(this.data);
    this.showSuccessMessage = true;
  }

  resetFavoriteMessage() {
    this.showSuccessMessage = false;
  }

  get username() {
    return this.WeatherService.getUserName();
  }

  updateInfo() {
    this.updateSubscription = this.WeatherService.getWeatherInfo(
      this.data.name
    ).subscribe(
      (info) => {
        this.WeatherService.triggerWeatherInfo(info);
      },
      (err) => {
        this.WeatherService.triggerErrorInfo('update failed !');
      }
    );
  }

  ngOnDestroy() {
    this.subscription ? this.subscription.unsubscribe() : null;
    this.updateSubscription ? this.updateSubscription.unsubscribe() : null;
  }
}
