import { Component, OnDestroy, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service';
import { cities } from '../../models/cities.modal';
import { WeatherResponse } from '../../models/weather-response.modal';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  cities!: cities[];
  weatherInfo!: WeatherResponse;

  private subscription!: Subscription;
  constructor(private WeatherService: WeatherService) {}

  ngOnInit(): void {
    this.getCities();
    this.updateUserDetails();
  }

  getCities() {
    this.subscription = this.WeatherService.getCities().subscribe((cities) => {
      this.cities = cities;
    });
  }

  getUserName() {
    return this.WeatherService.getUserName();
  }

  updateUserDetails() {
    if (sessionStorage && sessionStorage.getItem('user')) {
      let data = sessionStorage.getItem('user');
      if (data) {
        let userObj = JSON.parse(data);
        this.WeatherService.setUserName(userObj.name);
      }
    }
  }

  ngOnDestroy() {
    this.subscription ? this.subscription.unsubscribe() : null;
  }
}
