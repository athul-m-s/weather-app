import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { SharedModule } from '../../../modules/shared/shared-module';
import { cities } from '../../../models/cities.modal';
import { WeatherService } from '../../../services/weather.service';
import { of, throwError } from 'rxjs';
import { WeatherResponse } from '../../../models/weather-response.modal';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let service: WeatherService;
  let responseCity: cities[] = [
    {
      id: 7289568,
      name: 'Palakkad district',
      state: '',
      country: 'IN',
      coord: {
        lon: 76.651001,
        lat: 10.775,
      },
    },
    {
      id: 1277333,
      name: 'Bengaluru',
      state: '',
      country: 'IN',
      coord: {
        lon: 77.603287,
        lat: 12.97623,
      },
    },
  ];

  let response: WeatherResponse[] = [
    {
      coord: {
        lon: 76.65,
        lat: 10.7833,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      base: 'stations',
      main: {
        temp: 26.32,
        feels_like: 26.32,
        temp_min: 26.32,
        temp_max: 31.03,
        pressure: 1008,
        humidity: 93,
      },
      visibility: 10000,
      wind: {
        speed: 5.66,
        deg: 255,
      },
      rain: {
        '1h': 2.05,
      },
      clouds: {
        all: 100,
      },
      dt: 1630915343,
      sys: {
        type: 2,
        id: 2040581,
        country: 'IN',
        sunrise: 1630889021,
        sunset: 1630933221,
        message: 123,
      },
      timezone: 19800,
      id: 1260728,
      name: 'Palakkad',
      cod: 200,
      time: '2021-09-06T08:02:24.112Z',
    },
    {
      coord: {
        lon: 76.2144,
        lat: 10.5276,
      },
      weather: [
        {
          id: 501,
          main: 'Rain',
          description: 'moderate rain',
          icon: '10d',
        },
      ],
      base: 'stations',
      main: {
        temp: 27.94,
        feels_like: 32.73,
        temp_min: 26.77,
        temp_max: 27.94,
        pressure: 1009,
        humidity: 85,
      },
      visibility: 10000,
      wind: {
        speed: 1.78,
        deg: 283,
      },
      rain: {
        '1h': 3.16,
      },
      clouds: {
        all: 100,
      },
      dt: 1630915349,
      sys: {
        type: 1,
        id: 9211,
        country: 'IN',
        sunrise: 1630889133,
        sunset: 1630933318,
        message: 123,
      },
      timezone: 19800,
      id: 1254187,
      name: 'Thrissur',
      cod: 200,
      time: '2021-09-06T08:02:29.490Z',
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchComponent],
      imports: [SharedModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(WeatherService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a form with one control', () => {
    expect(component.searchForm.contains('city')).toBeTruthy();
  });

  it('Should reset errors when reset error called', () => {
    component.showError = true;
    component.errorMessage = 'error';
    component.resetError();
    expect(component.showError).toBeFalse();
    expect(component.errorMessage).toEqual('');
  });

  it('Should call getWeatherInfo when user clicks search button', () => {
    spyOn(service, 'getWeatherInfo').and.returnValue(of(response[0]));
    component.onSubmit(responseCity[0].name);
    expect(service.getWeatherInfo).toHaveBeenCalled();
  });

  it('Should show error message when getWeatherInfo return error', () => {
    const testError = {
      status: 404,
      error: {
        message: 'city not found',
      },
    };
    spyOn(service, 'getWeatherInfo').and.returnValue(throwError(testError));
    component.onSubmit(responseCity[0].name);
    expect(component.showError).toBeTruthy();
  });

  it('Should show error message when getWeatherInfoByCoordinates return error', () => {
    const testError = {
      status: 404,
      error: {
        message: 'city not found',
      },
    };

    const coordinates = { lat: 1, lon: 2 };
    spyOn(service, 'getWeatherInfoByCoordinates').and.returnValue(
      throwError(testError)
    );
    component.searchByLocation(coordinates);
    expect(component.showError).toBeTruthy();
  });

  it('Should call getWeatherInfoByCoordinates when user clicks search by co-ordinates button', () => {
    spyOn(service, 'getWeatherInfoByCoordinates').and.returnValue(
      of(response[0])
    );
    let mockData = {
      lat: 123,
      lon: 234,
    };
    component.searchByLocation(mockData);
    expect(service.getWeatherInfoByCoordinates).toHaveBeenCalled();
  });

  it('Should clear data when searchCleared method calls', () => {
    component.data = responseCity;
    component.searchCleared();
    expect(component.data.length).toBe(0);
  });

  it('Should clear data when onFocused method calls', () => {
    component.data = responseCity;
    component.onFocused(null);
    expect(component.data.length).toBe(0);
  });

  it('Should reset form , clear errors , and call onsubmit when selectEvent method calls', () => {
    component.searchForm.get('city')?.setValue('london');
    spyOn(component, 'onSubmit');
    spyOn(component, 'resetError');

    component.selectEvent(responseCity[0]);

    expect(component.onSubmit).toHaveBeenCalled();
    expect(component.resetError).toHaveBeenCalled();
    expect(component.searchForm.get('city')?.value).toEqual(null);
  });

  it('Should reset form , clear errors , and call onsubmit when onSearch method calls', () => {
    component.searchForm.get('city')?.setValue('london');
    spyOn(component, 'onSubmit');
    spyOn(component, 'resetError');

    component.onSearch();

    expect(component.onSubmit).toHaveBeenCalled();
    expect(component.resetError).toHaveBeenCalled();
    expect(component.searchForm.get('city')?.value).toEqual(null);
  });

  it('Should subscribe to weatherUpdateSub on ngOnInit', () => {
    const nextSpy = spyOn(service.weatherUpdateSub, 'subscribe');

    component.ngOnInit();
    service.triggerWeatherInfo(response[0]);

    expect(nextSpy).toHaveBeenCalled();
  });

  it('Should subscribe to errorUpdateSub on ngOnInit', () => {
    const nextSpy = spyOn(service.errorUpdateSub, 'subscribe');

    component.ngOnInit();
    service.triggerErrorInfo('error');

    expect(nextSpy).toHaveBeenCalled();
  });

  it('Should show city suggestions when user enters input', () => {
    component.showError = false;
    component.cities = responseCity;
    component.onChangeSearch('Bengaluru');
    expect(component.data).toContain(responseCity[1]);
  });

  it('Should show city suggestions when user enters input', () => {
    component.showError = false;
    component.cities = responseCity;
    component.onChangeSearch('abcdefgh');
    expect(component.data.length).toEqual(0);
  });

  it('Should reset already shown error when user enters input', () => {
    component.cities = responseCity;
    component.showError = true;
    let spy = spyOn(component, 'resetError');
    component.onChangeSearch('cityName');
    expect(spy).toHaveBeenCalled();
  });
});
