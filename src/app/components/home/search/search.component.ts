import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { cities } from '../../../models/cities.modal';
import { WeatherService } from '../../../services/weather.service';
import { Coordinates } from '../../../models/coordinates.modal';
import { WeatherResponse } from '../../../models/weather-response.modal';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit, OnDestroy {
  @Input() cities!: cities[];
  @Output() response = new EventEmitter<WeatherResponse>();
  searchForm!: FormGroup;
  keyword = 'name';
  data: cities[] = [];
  coords!: Coordinates;
  showError: boolean = false;
  errorMessage: string = '';

  private subscription!: Subscription;
  private errSubscription!: Subscription;
  private updateSubscription!: Subscription;

  constructor(private WeatherService: WeatherService, private fb: FormBuilder) {
    this.searchForm = this.createSearchForm();
  }

  ngOnInit(): void {
    this.updateSubscription = this.WeatherService.weatherUpdateSub.subscribe(
      (data) => {
        this.resetError();
        this.response.emit(data);
      }
    );

    this.errSubscription = this.WeatherService.errorUpdateSub.subscribe(
      (error) => {
        this.showError = true;
        this.errorMessage = error;
      }
    );
  }

  selectEvent(item: cities) {
    this.resetError();
    this.onSubmit(item.name);
    this.searchForm.reset();
  }

  onChangeSearch(val: string) {
    if (this.showError) {
      this.resetError();
    }
    let arr = this.cities.filter((x) => {
      return x.name.trim().toLowerCase().includes(val.toLowerCase().trim());
    });
    this.data = arr.slice(0, 10);
  }

  searchCleared() {
    this.data = [];
  }

  onFocused(e: any) {
    this.data = [];
  }

  onSearch() {
    this.resetError();
    let city: string = this.searchForm.get('city')?.value;
    this.onSubmit(city.trim());
    this.searchForm.reset();
  }

  onSubmit(city: string) {
    this.subscription = this.WeatherService.getWeatherInfo(city).subscribe(
      (data) => {
        if (data) {
          this.response.emit(data);
        } else {
        }
      },
      (error) => {
        this.showError = true;
        this.errorMessage = error?.error.message;
      }
    );
  }

  getLocation() {
    this.searchForm.reset();
    this.resetError();
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          this.coords = { lat: latitude, lon: longitude };
          this.searchByLocation(this.coords);
        },
        (error: GeolocationPositionError) => {
          this.showError = true;
          this.errorMessage = error.message;
        }
      );
    } else {
    }
  }

  searchByLocation(coords: Coordinates) {
    this.subscription = this.WeatherService.getWeatherInfoByCoordinates(
      coords
    ).subscribe(
      (data) => {
        if (data) {
          this.response.emit(data);
        } else {
        }
      },
      (error) => {
        this.showError = true;
        this.errorMessage = error?.error.message;
      }
    );
  }

  resetError() {
    this.showError = false;
    this.errorMessage = '';
  }

  createSearchForm() {
    return this.fb.group({
      city: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
    });
  }

  ngOnDestroy() {
    this.subscription ? this.subscription.unsubscribe() : null;
    this.errSubscription ? this.errSubscription.unsubscribe() : null;
    this.updateSubscription ? this.updateSubscription.unsubscribe() : null;
  }
}
